﻿var http = require('http'), 
    request = require("request"),
    weighted = require('./balacers/weightedBalancer.js'),
    balancerRoundRobin = require('./balacers/balancerRoundRobin.js'),
    weighedOn = require('./balacers/balancerReqCount.js'),
    stiky = require('./balacers/sickySession.js'),
    _ = require('underscore'),
    cookie = require('cookie-parser'),
    express = require("express");


/* proxypass(proxyConfig)
 * 
 * return an express.Router() given a proxy configuration object
 * 
 * <proxyConfig> : can have the following properties
 * {
 *      proxyName : <string> identifier of the proxy
 *      proxyUrl : <string URL> | <RegExpr> entry point that will be served by the
 *                 workers of this proxy
 *      workers : <[workerObject]> list of worker of the proxy
 *      balancer: <balancerObject> balancer used by the proxy to split the load among the workers
 *  }
 * 
 *  Note : balancer property is optional, if missing per request round robin
 *  balancer is used
 *  
 *               
 * <workerObject> : worker configuration object, can have the following properties
 * {
 *      target : URL of the worker 
 *      optional properties needed by the specific balancer
 * }
 *      
 * <balancerObject> : can be one of the provided balancer or a custom one.
 *      To use a custom balancer you have to provide a function constructor that
 *      create the balancer, this will be called with the list of the workers as argument
 *      
 *      Examples. 
 *      constructor(workers);
 *      
 *      it has to return a function or an object with a selectWorker method.
 *      To this method are passed the request object and the response object
 *      and has to return the url of the worker that will serve the current request
 *      
 *      Examples. 
 *      balancer(req, res) | balancer.selectWorker(req, res)
 */

//proxypass :: proxyConfiguration -> expressRouter
proxypass = function (proxyConfig) {
    var router = express.Router();
    
    if (_.any(proxyConfig.workers, function (w) { return false === (w.target || false); })) {
        throw new Error('all workers must have a "target" property');
    }

    var entryPoint = proxyConfig.proxyUrl || false;
    if (!entryPoint) {
        throw new Error("proxyUrl property is missing");
    }

    if (!(proxyConfig.proxyName || false)) {
        throw new Error("proxyName property is missing");
    }

    //if the entrypoint is a string transform it in regexp
    var regexEntryPoint = _.isRegExp(entryPoint) ? entryPoint : new RegExp('^' + entryPoint + '/?$');
    
    router.use(regexEntryPoint, function (req, res, next) {
        //set a porxy property in the request object with the name of the proxy
        //that will serve the request
        req.proxy = proxyConfig.proxyName;
        next();
    });

    return router;
}

/* createProxy(option)
 * 
 * given an option object return the proxy application (express application)
 * 
 * <option> : [proxyConfig] mandatory, list of proxy configuration used by the application
 * 
 * the application returned is a function, so can be passed to an httpServer or
 * you can use the listen method that is a shortcut to the httpServer.listen method
 * 
 * Examples.
 *      http.createServer(nod_proxy.createProxy(option)).listen(port);
 *      nod_proxy.createProxy(option).listen(port);
 */

//createProxy :: option -> app
createProxy = function (option){
    var app = express();
    
    //to parse the request's cookies
    app.use(cookie());
    
    //for each proxyObject an express.Router will be created
    var routers = _.map(option, function (proxy) { return proxypass(proxy); });
    app.use(routers);
    
    //binding table that map to each proxy the corresponding balancer
    var balancerTable = _.object(_.map(option, function (proxy) {
        //round robin as default balancer
        var proxybalancerConstructor = proxy.balancer || balancerRoundRobin;
        var proxybalancer = proxybalancerConstructor(proxy.workers);
        
        var serveRequest = false;
        if (_.isFunction(proxybalancer)) {
            serveRequest = proxybalancer;
        } else {
            serveRequest = proxybalancer.selectWorker || false;

            if (!serveRequest) {
                throw new Error("the provided balancer is not a function or it doesn't have a selectWorker method");
            }
        }

        return [proxy.proxyName, serveRequest];
    }));
    
    app.use('*', function (req, res) {      
        var whichProxy = req.proxy || false;
        
        if (whichProxy) {
            forwardRequest(req, res, balancerTable[whichProxy](req, res));
        } else {
            res.status(404).send();
        }
    });

    return app;
}


/* forward the incoming request to the selected worker and send
 * back the worker's response
 */
forwardRequest = function (req, res , destination){
    req.pipe(
        request(destination)
        .on("error", function (err) {
            //server not available
            res.status(503).send('service not available');
        })
    ).pipe(res);
}


// Module exports
exports.createProxy = createProxy;

// Balancer included:

// Round robin per request O(1)
exports.balancerRR = balancerRoundRobin;
// Statistical weighted balancer O(1)
exports.balancerWeighted = weighted;
// Sticky session balancer O(1)
exports.balancerStiky = stiky;
// Weighted balancer O(n)
exports.balancerWeightedOn = weighedOn;