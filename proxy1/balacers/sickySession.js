﻿var rr = require('./balancerRoundRobin.js'),
    _ = require('underscore');


/* This balancer provide stikyness, if a session is present all following
 * request from the same user are proxyed to the same back-end.
 * It can use a session cookie or URL encoding, the session cookie can be set
 * by the back-end or by the proxy itself.
 * 
 * Some back-ends use a slightly different form of stickyness cookie, 
 * for instance Apache Tomcat. Tomcat adds the name of the Tomcat instance 
 * to the end of its session id cookie, separated with a dot (.) 
 * from the session id. Thus if the proxy finds a dot 
 * in the value of the stickyness cookie, it only uses the part 
 * behind the dot to search for the route.
 * 
 * If the stikyness is implemented using url encoding the proxy searches
 * for the query parameter in the request.
 * The Java standards implement URL encoding slightly different. 
 * They use a path info appended to the URL using a semicolon (;) 
 * as the separator and add the session id behind.
 * 
 * You can specify the stikysession parameter's name either for the url
 * encoding and the session cookie. If both are present the url encoding has
 * higher priority.
 * You can also specify the path delimiter for both url encoding and 
 * cookie session, the default is set to dot (.)
 * 
 * 
 * You have to provide a option object to the balancer to configure it.
 * 
 * Example.
 *      {
 *        proxyName : 'p1', 
 *        proxyUrl : '/', 
 *        workers : [{ target : "http://127.0.0.1:1337" , route : "srv1" },
 *                   { target : "http://127.0.0.1:1338" , route : "srv2" }],
 *                   
 *        balancer : nod_proxy.balancerStiky({ 
 *              sessionUrl : 'mysessionid' ,
 *              pathDelimiter : ';'
 *              })
 *      }
 * 
 * Note.
 *      All workers must have a "route" property which is compared to the
 *      request's stikysession value.
 *      
 * 
 * The option object can have the following properties
 *     {
 *      sessionUrl : <string> stikysession's parameter name for url encoding
 *      sessionCookie : <string> stikysession's parameter name for session cookie
 *      pathDelimiter : <string> separator character for url encoding
 *      cookiePathDelimiter : <string> separator character for session cookie
 *      setSessionCookie : <bool> if true the proxy set a session cookie
 *                         if no session is found
 *     }
 *     
 * Note.
 *      - You must provide at least sessionUrl or sessionCookie
 *      - pathDelimiter is optional, default is set to dot (.)
 *      - cookiePathDelimiter is optional, default is set to dot (.)
 *      - setSessionCookie is optional, default is set to false
 */
module.exports = function (option) {

    var urlName = option.sessionUrl || false;
    var cookieName = option.sessionCookie || false;
    
    if (!cookieName && !urlName) {
        throw new Error('must specify at least "sessionCookie" property or "sessionUrl" property')
    }
    
    //path delimiter for URL encoding default delimiter "."
    var separatorUrl = option.pathDelimiter || '.';
    //path delimiter for sessionCookie default delimiter "."
    var separatorCookie = option.cookiePathDelimiter || '.';
    
    var setCookie = option.setSessionCookie || false;
    
    /* extract the route info from the session string,
     * you can provide the path delimiter
     * 
     * Example
     * "123a.srv1"  ->  "srv1"
     * "srv1"  -> "srv1"
     */
    var getRoute = function (sessionString, pathDelim){
        if (sessionString.indexOf(pathDelim) >= 0)
            return sessionString.split(pathDelim)[1];
        else
            return sessionString;
    }
    
    // return the balancer constructor
    return function (workers) {
        
        if (_.any(workers, function (w) { return false === (w.route || false); })) {
            throw new Error('all workers must have a "route" property');
        }
        
        var balancerRR = rr(workers);
        
        var routeTable = _.object(_.map(workers, function (w) { 
            return [w.route, w.target];
        }));
        
        /* select the worker if no session information is found,
         * if specified set the session
         */
        var serveFreshRequest = function (res) {
            //select the worker
            var newtarget = balancerRR();
            
            //set the session cookie if specified
            if (setCookie) {
                res.cookie(cookieName, 
                            _.find(_.keys(routeTable), function (k) { return routeTable[k] === newtarget; })
                );
            }
            return newtarget;
        }

        var selectNext = function (req, res) {
            var session = false;

            if (urlName) {
                session = req.query[urlName] || false;
                var separator = separatorUrl;
            } 
            if(cookieName && !session) {
                session = req.cookies[cookieName] || false;
                var separator = separatorCookie;
            }

            if (session) {
                var requestRoute = getRoute(session, separator);

                var selectedW = routeTable[requestRoute] || false;
                if (!selectedW) {
                    return serveFreshRequest(res);
                }
                else {
                    return selectedW;
                }
            }
            else {
                return serveFreshRequest(res);
            }
        }
        
        return selectNext;
    }
}