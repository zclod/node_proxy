﻿/* Balancer round robin per request, O(1) complexity
 * 
 * Example.
 * 
 * var p1 = {
 *  proxyName : "p1",
 *  proxyUrl : "/",
 *  workers : [wrk1, wrk2], 
 *  balancer : proxy.balancerRR
 * }
 * 
 */

module.exports = (function (workers) {
    var workerList = workers;
    var currWorker = workerList.length - 1;
    
    var selecNext = function () {
        currWorker = currWorker + 1;
        currWorker %= workerList.length;
        return workerList[currWorker].target;
    }
    
    return selecNext;
})