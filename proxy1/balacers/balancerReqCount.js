﻿var _ = require('underscore');


/* Weighted request distribution among workers, O(n) complexity.
 * This is the same algorithm used by Apache mod_proxy whith
 * "Request counting algorithm" (lbmethod=byrequest)
 * http://httpd.apache.org/docs/2.2/mod/mod_proxy_balancer.html
 * 
 * Each worker must have a "loadFactor" property that represent the
 * worker's work quota. 
 * 
 * This value is normalized so
 * 
 * { target : "http://127.0.0.1:1337" , loadFactor : 1 }
 * { target : "http://127.0.0.1:1338" , loadFactor : 1 }
 * 
 * is equal to
 * { target : "http://127.0.0.1:1337" , loadFactor : 12 }
 * { target : "http://127.0.0.1:1338" , loadFactor : 12 } 
 * 
 * Examples.
 * 
 *      var proxy = {
 *        workers : [ { target : "http://127.0.0.1:1337" , loadFactor : 7 }
 *                  , { target : "http://127.0.0.1:1338" , loadFactor : 3 }], 
 *        proxyName : "p1", 
 *        proxyUrl : '/', 
 *        balancer : proxy.balancerWeightedOn
 *      }
 */ 

module.exports = (function (workers) {
    if (_.any(workers, function (w) { return false === (w.loadFactor || false); })) {
        throw new Error('all workers must have a "loadFactor" property');
    }
    
    for (w in workers) {
        workers[w].lbstatus = 0;
    }

    var selecNext = function () {
        var candidate = 0;
        var totalLoad = 0;
        for (w in workers) {
            workers[w].lbstatus += workers[w].loadFactor;
            totalLoad += workers[w].loadFactor;
            if (workers[w].lbstatus > workers[candidate].lbstatus) {
                candidate = w;
            }
        }

        workers[candidate].lbstatus -= totalLoad;

        return workers[candidate].target;

    }
    
    return selecNext;
})