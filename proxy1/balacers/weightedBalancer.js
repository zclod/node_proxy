﻿var _ = require('underscore');


/* Statistical request distribution among workers O(1) complexity
 * 
 * Each worker must have a "loadFactor" property that represent the
 * worker's work quota. 
 * 
 * This value is normalized so
 * 
 * { target : "http://127.0.0.1:1337" , loadFactor : 1 }
 * { target : "http://127.0.0.1:1338" , loadFactor : 1 }
 * 
 * is equal to
 * { target : "http://127.0.0.1:1337" , loadFactor : 12 }
 * { target : "http://127.0.0.1:1338" , loadFactor : 12 } 
 * 
 * Examples.
 * 
 *      var proxy = {
 *        workers : [ { target : "http://127.0.0.1:1337" , loadFactor : 7 }
 *                  , { target : "http://127.0.0.1:1338" , loadFactor : 3 }], 
 *        proxyName : "p1", 
 *        proxyUrl : '/', 
 *        balancer : proxy.balancerWeighted
 *      }
 */ 
module.exports = function (workers){
    if (_.any(workers, function (w) { return false === (w.loadFactor || false); })) {
        throw new Error('all workers must have a "loadFactor" property');
    }

    var totalLoad = _.reduce(workers, function (memo, worker) { return memo + worker.loadFactor; }, 0);
    
    var workerSample = [];
    for (w in workers) {
        for (var i = 0; i < workers[w].loadFactor; i++) {
            workerSample.push(workers[w]);
        }
    }
    
    workerSample = _.shuffle(workerSample);

    var selectNext = function (){
        var r = Math.floor(Math.random() * totalLoad);
        return workerSample[r].target;
    }

    return selectNext;

}