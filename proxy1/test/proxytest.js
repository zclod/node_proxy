﻿var should = require('should');
var rewire = require('rewire');
var nod_proxy = rewire('../proxy.js');
var http = require('http');
var request = require('request');


var srv1 = http.createServer(function (req, res) {
    res.writeHead(200, { 'Content-Type': 'text/plain' });
    res.end('server 1337');
});

var srv2 = http.createServer(function (req, res) {
    res.writeHead(200, { 'Content-Type': 'text/plain' });
    res.end('server 1338');
});

var srv3 = http.createServer(function (req, res) {
    res.writeHead(200, { 'Content-Type': 'text/plain' });
    res.end('server 1339');
});

var srv4 = http.createServer(function (req, res) {
    res.writeHead(200, { 'Content-Type': 'text/plain' });
    res.end('regexp .*abc.*');
});


var trg1 = { target : "http://127.0.0.1:1337" , route : "srv1"}
var trg2 = { target : "http://127.0.0.1:1338" , route : "srv2"}
var trg3 = { target : "http://127.0.0.1:1339" , route : "srv3"}
var trg4 = { target : "http://127.0.0.1:1340" }


var p1 = {
    workers : [trg1, trg2], 
    proxyName : "p1", 
    proxyUrl : "/", 
    balancer : nod_proxy.balancerRR
}
var p2 = {
    proxyName : "p2",
    proxyUrl : "/prova", // /\/prova/,  javascript notation
    workers : [trg3], 
    balancer : nod_proxy.balancerRR
}
var p3 = {
    proxyName : 'p3',
    proxyUrl : /.*abc.*/,
    workers : [trg4]
}

var proxy = nod_proxy.createProxy([p1, p2, p3]);
var proxySrv = http.createServer(proxy);

describe('nod_proxy core function', function () {
    
    describe('.proxypass(proxyConfig)', function () {
        it('should throw an error if the argument doesn\'t have a proxyName property', function () {
            var p = { proxyUrl : '/' };
            (function () { nod_proxy.__get__('proxypass')(p); }).should.throw(/proxyName/);
            p.proxyName = 'a';
            (function () { nod_proxy.__get__('proxypass')(p); }).should.not.throw();
        });

        it('should throw an error if the argument doesn\'t have a proxyUrl property', function () {
            var p = { proxyName : 'a' };
            (function () { nod_proxy.__get__('proxypass')(p); }).should.throw(/proxyUrl/);
            p.proxyUrl = '/';
            (function () { nod_proxy.__get__('proxypass')(p); }).should.not.throw();
        });

        it('should throw an error if any of the workers is missing the target property', function () {
            var p = { proxyName : 'a', proxyUrl : '/', workers : [trg1, trg2, {}] };
            (function () { nod_proxy.__get__('proxypass')(p); }).should.throw(/target/);
            p.workers = [trg1, trg2];
            (function () { nod_proxy.__get__('proxypass')(p); }).should.not.throw();
        });
    });

    //describe('.createProxy(option)', function () {
    //    it('should throw an error if the balancer provided is not a valid balancer', function () {
    //        var dummybalancer = function () { return function () { } };

    //        var pfake = {
    //            proxyName : 'p3',
    //            proxyUrl : /.*abc.*/,
    //            workers : [trg4] 
    //            //,balancer : dummybalancer
    //        }
    //        (function () { nod_proxy.createProxy([pfake]); }).should.not.throw();
    //    });
    //});

});

describe('proxy server with given configuration', function () {
    before(function () {
        srv1.listen(1337);
        srv2.listen(1338);
        srv3.listen(1339);
        srv4.listen(1340);
        proxySrv.listen(8080);
    });
    
    after(function () {
        srv1.close();
        srv2.close();
        srv3.close();
        srv4.close();
        proxySrv.close();
    });

    describe('/ : balancer round robin ("server 1337", "server 1338")', function () {
        var endpoint = "http://localhost:8080";

        it('should return "server 1337"', function (done) {
            request(endpoint, function (err, res, body) {
                body.should.equal("server 1337");
                done();
            });
        });

        it('should return "server 1338"', function (done) {
            request(endpoint, function (err, res, body) {
                body.should.equal("server 1338");
                done();
            });
        });

        it('should return "server 1337" (start over)', function (done) {
            request(endpoint, function (err, res, body) {
                body.should.equal("server 1337");
                done();
            });
        });

        it('should return http 200', function (done) {
            request(endpoint, function (err, res, body) {
                res.statusCode.should.equal(200);
                done();
            });
        });
    });

    describe('/prova : "server 1339"', function () {
        var endpoint = "http://localhost:8080/prova";
        
        it('should return http 200', function (done) {
            request(endpoint , function (err, res, body) {
                res.statusCode.should.equal(200);
                done();
            });
        });

        it('should return "server 1339"', function (done) {
            request(endpoint , function (err, res, body) {
                body.should.equal("server 1339");
                done();
            });
        });

        it('/prova should be the same as /prova/', function (done) {
            request(endpoint + '/' , function (err, res, body) {
                res.statusCode.should.equal(200);
                body.should.equal("server 1339");
                done();
            });
        });

        it('if the server is down should return http 503', function (done) {
            srv3.close();
            
            request(endpoint, function (err, res, body) {
                res.statusCode.should.equal(503);
                done();
            });

            //srv3.listen(1339);
        });

        it('/prova/something should return 404', function (done) {
            request(endpoint + '/something' , function (err, res, body) {
                res.statusCode.should.equal(404);
                done();
            });
        });

    });

    describe('.*abc.* : match the given regexp', function () {
        var endpoint = "http://localhost:8080";

        it('/abc should return http 200', function (done) {
            request(endpoint + '/abc' , function (err, res, body) {
                res.statusCode.should.equal(200);
                done();
            });
        });
        
        it('/abcdef should return http 200', function (done) {
            request(endpoint + '/abcdef' , function (err, res, body) {
                res.statusCode.should.equal(200);
                done();
            });
        });

        it('/something/abc should return http 200', function (done) {
            request(endpoint + '/something/abc' , function (err, res, body) {
                res.statusCode.should.equal(200);
                done();
            });
        });

        it('/something/abc/something should return http 200', function (done) {
            request(endpoint + '/something/abc/something' , function (err, res, body) {
                res.statusCode.should.equal(200);
                done();
            });
        });
    });
});
