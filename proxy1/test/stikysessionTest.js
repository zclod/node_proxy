﻿var should = require('should');
var rewire = require('rewire');
var nod_proxy = rewire('../proxy.js');
var http = require('http');
var request = require('request');


var srv1 = http.createServer(function (req, res) {
    res.writeHead(200, { 'Content-Type': 'text/plain' });
    res.end('server 1337');
});

var srv2 = http.createServer(function (req, res) {
    res.writeHead(200, { 'Content-Type': 'text/plain' });
    res.end('server 1338');
});

var srv3 = http.createServer(function (req, res) {
    res.writeHead(200, { 'Content-Type': 'text/plain' });
    res.end('server 1339');
});

var srv4 = http.createServer(function (req, res) {
    res.writeHead(200, { 'Content-Type': 'text/plain' });
    res.end('regexp .*abc.*');
});


var trg1 = { target : "http://127.0.0.1:1337" , route : "srv1" }
var trg2 = { target : "http://127.0.0.1:1338" , route : "srv2" }
var trg3 = { target : "http://127.0.0.1:1339" , route : "srv3" }
var trg4 = { target : "http://127.0.0.1:1340" }


var p1 = {
    workers : [trg1, trg2], 
    proxyName : "p1", 
    proxyUrl : "/", 
    balancer : nod_proxy.balancerRR
}
var p2 = {
    proxyName : "p2",
    proxyUrl : "/prova", // /\/prova/,  javascript notation
    workers : [trg3], 
    balancer : nod_proxy.balancerRR
}
var p3 = {
    proxyName : 'p3',
    proxyUrl : /.*abc.*/,
    workers : [trg4]
}


describe('stikysession balancer', function () {
    var endpoint = 'http://localhost:8080';
    var p;
    
    before(function () {
        srv1.listen(1337);
        srv2.listen(1338);
        srv3.listen(1339);
        srv4.listen(1340);
            
    });
    
    after(function () {
        srv1.close();
        srv2.close();
        srv3.close();
        srv4.close();
            
    });
    
    afterEach(function () {
        p.close();
    });
    
    it('should use round robin if no session is found', function (done) {
        p = nod_proxy.createProxy([{
                proxyName : 'p1', 
                proxyUrl : '/', 
                workers : [trg1, trg2, trg3], 
                balancer : nod_proxy.balancerStiky({ sessionCookie : 'mysessionid' })
            }]).listen(8080);
        
        request(endpoint, function (err, res, body) {
            res.body.should.equal('server 1337');
            request(endpoint, function (err, res, body) {
                res.body.should.equal('server 1338');
                request(endpoint, function (err, res, body) {
                    res.body.should.equal('server 1339');
                    done();
                });
            });
        });
    });
    
    it('session cookie test', function (done) {
        p = nod_proxy.createProxy([{
                proxyName : 'p1', 
                proxyUrl : '/', 
                workers : [trg1, trg2, trg3], 
                balancer : nod_proxy.balancerStiky({ sessionCookie : 'mysessionid' })
            }]).listen(8080);
        
        
        var j;
        var c;
        
        j = request.jar();
        c = request.cookie('mysessionid=1234.srv2');
        j.setCookie(c, endpoint);
        request({ url : endpoint, jar : j }, function (err, res, body) {
            res.body.should.equal('server 1338');
            j = request.jar();
            c = request.cookie('mysessionid=1234.srv1');
            j.setCookie(c, endpoint);
            request({ url : endpoint, jar : j }, function (err, res, body) {
                res.body.should.equal('server 1337');
                j = request.jar();
                c = request.cookie('mysessionid=1234.srv3');
                j.setCookie(c, endpoint);
                request({ url : endpoint, jar : j }, function (err, res, body) {
                    res.body.should.equal('server 1339');
                    done();
                });
            });
        });
    });
    
    it('session url encoded test', function (done) {
        p = nod_proxy.createProxy([{
                proxyName : 'p1', 
                proxyUrl : '/', 
                workers : [trg1, trg2, trg3], 
                balancer : nod_proxy.balancerStiky({ sessionUrl : 'mysessionid', pathDelimiter : ';' })
            }]).listen(8080);
        
        var r = require('request');
        
        r(endpoint + '?mysessionid=1234;srv2', function (err, res, body) {
            res.body.should.equal('server 1338');
            r(endpoint + '?mysessionid=1234;srv2', function (err, res, body) {
                res.body.should.equal('server 1338');
                r(endpoint + '?mysessionid=1234;srv1', function (err, res, body) {
                    res.body.should.equal('server 1337');
                    r(endpoint + '?mysessionid=1234;srv3', function (err, res, body) {
                        res.body.should.equal('server 1339');
                        done();
                    });
                });
            });
        });
    });
    
    it('url encoded session has higher priority than cookie', function (done) {
        p = nod_proxy.createProxy([{
                proxyName : 'p1', 
                proxyUrl : '/', 
                workers : [trg1, trg2, trg3], 
                balancer : nod_proxy.balancerStiky({ sessionCookie : 'mysessionid' , sessionUrl : 'MYID', pathDelimiter : ';' })
            }]).listen(8080);
        
        var j;
        var c;
        
        j = request.jar();
        c = request.cookie('mysessionid=1234.srv2');
        j.setCookie(c, endpoint);
        request({ url : endpoint + '?MYID=1234;srv3', jar : j }, function (err, res, body) {
            res.body.should.equal('server 1339');
            j = request.jar();
            c = request.cookie('mysessionid=1234.srv2');
            j.setCookie(c, endpoint);
            request({ url : endpoint, jar : j }, function (err, res, body) {
                res.body.should.equal('server 1338');
                done();
            });
        });
    });
    
    it('if passed an invalid session should ignore it and use round robin', function (done) {
        p = nod_proxy.createProxy([{
                proxyName : 'p1', 
                proxyUrl : '/', 
                workers : [trg1, trg2, trg3], 
                balancer : nod_proxy.balancerStiky({
                    sessionCookie : 'mysessionid' , sessionUrl
                         : 'MYID', pathDelimiter : ';'
                })
            }]).listen(8080);
        
        request(endpoint + '?MYID=1234;srv4', function (err, res, body) {
            res.body.should.equal('server 1337');
            done();
        });
    });
    
    it('setSessionCookie test : if a session is not present the proxy should set it', function (done) {
        p = nod_proxy.createProxy([{
                proxyName : 'p1', 
                proxyUrl : '/', 
                workers : [trg1, trg2, trg3], 
                balancer : nod_proxy.balancerStiky({
                    sessionCookie : 'mysessionid' , setSessionCookie : true
                })
            }]).listen(8080);
        
        var j = request.jar();
        request({ url : endpoint, jar : j }, function (err, res, body) {
            var cookies = j.getCookies('http://localhost');
            cookies[0].value.should.equal('srv1');
            request({ url : endpoint, jar : j }, function (err, res, body) {
                var cookies = j.getCookies('http://localhost');
                cookies[0].value.should.equal('srv1');
                done();
            });
        });
    });

    it('should throw an error if "sessionCookie" and "sessionURL" properties are not specified', function () {
        var option = { prova: 1 };
        (function () { nod_proxy.balancerStiky(option); }).should.throw(/specify/);
        option.sessionCookie = 'j';
        (function () { nod_proxy.balancerStiky(option); }).should.not.throw();
    });
    
    it('should throw an error if any worker miss the route property', function () {
        var option = { sessionCookie : 'JSESSIONID' };
        (function () {
            var stiky = nod_proxy.balancerStiky(option);
            return stiky([trg1, trg2, trg3, trg4]);
        }).should.throw();
        
        (function () {
            var stiky = nod_proxy.balancerStiky(option);
            return stiky([trg1, trg2, trg3]);
        }).should.not.throw(/route/);
    });
});
