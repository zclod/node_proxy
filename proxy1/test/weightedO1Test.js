﻿var nod_proxy = require('../proxy.js');
var should = require('should');

var trg1 = { target : "http://127.0.0.1:1337" , loadFactor : 3 }
var trg2 = { target : "http://127.0.0.1:1338" , loadFactor : 3 }
var trg3 = { target : "http://127.0.0.1:1339" , loadFactor : 7 }
var trg4 = { target : "http://127.0.0.1:1340" }


describe('Statistical weighted balancer', function () {
    
    it('should throw an error if "loadFactor" property is not specified', function () {
        (function () { nod_proxy.balancerWeighted([trg1, trg2, trg4]); }).should.throw(/load/);
        (function () { nod_proxy.balancerWeighted([trg1, trg2, trg3]); }).should.not.throw();
    });
    
    it('load factor 3-7 test, the work should be splitted around 30% - 70%', function () {
        var balancer = nod_proxy.balancerWeighted([trg1, trg3]);
        var counter ={ "http://127.0.0.1:1337" : 0 , "http://127.0.0.1:1339" : 0};
        
        var max = 1000;
        for (var i = 0; i < max; i++) {
            counter[balancer()]++;
        }
        
        for (k in counter) {
            console.log(k + ' split ratio : ' + counter[k] / max * 100 + '%');
        }

        (counter["http://127.0.0.1:1337"] / max * 100 ).should.be.within(20, 40);
        (counter["http://127.0.0.1:1339"] / max * 100).should.be.within(60, 80);
    });

    it('load factor 3-3 test, the work should be splitted around 50% - 50%', function () {
        var balancer = nod_proxy.balancerWeighted([trg1, trg2]);
        var counter = { "http://127.0.0.1:1337" : 0 , "http://127.0.0.1:1338" : 0 };
        
        var max = 1000;
        for (var i = 0; i < max; i++) {
            counter[balancer()]++;
        }
        
        for (k in counter) {
            console.log(k + ' split ratio : ' + counter[k] / max * 100 + '%');
        }
        
        (counter["http://127.0.0.1:1337"] / max * 100).should.be.within(40, 60);
        (counter["http://127.0.0.1:1338"] / max * 100).should.be.within(40, 60);
    });
});
